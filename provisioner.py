import json
import requests


def get_hotel_code_list():
    # hcl = hotel code list
    hcl = []
    file = 'path' # Path of file that includes hotel codes
    with open(file, 'r') as f:
        for i in range(0, 1000):
            hcl.append(f.readline())

    hcl = map(lambda s: s.strip(), hcl)

    return hcl


class Provisi(object):
    def __init__(self, params):
        self.search_url = 'http://localhost:8000/api/v2/search/?'
        self.avail_url = 'http://localhost:8000/api/v2/availability/'
        self.prov_url = 'http://localhost:8000/api/v2/provision/'
        self.params = params
        self.hotel_code_list = []  # It stores hotels of Hoteldo

    def search(self): # search step
        url = self.search_url

        for i, key in enumerate(self.params.keys()):
            url += '%s=%s' % (key, self.params[key])

            if i < len(self.params.keys()) - 1:
                url += '&'

        response = requests.get(url, auth=('username', 'password'))

        if response.status_code == 200:
            return response
        else:
            return None

    def avail(self): # availability step
        response = self.search()

        python_obj = response.json()
        if python_obj['count'] != 0:
            code = python_obj['results'][0]['products'][0]['code']
            se_price = python_obj['results'][0]['products'][0]['price']

            url = self.avail_url
            url = url + code
            response = requests.get(url, auth=('username', 'password'))

            if response.status_code == 200:  # if product is available
                return 1, code, se_price
            else:  # if product is not available
                return 0, 0, 0
        else:  # if there is no search response
            return 0, 0, 0

    def provision(self): # provision step

        is_response, code, avail_price = self.avail()
        if is_response:
            print 'Search price:', avail_price
            url = self.prov_url
            url = url + code

            response = requests.post(url, auth=('username', 'password'))
            return response, avail_price
        else:
            return None, None


if __name__ == '__main__':

    params = {'checkin': '2016-11-11', 'checkout': '2016-11-14',
              'pax': '2',
              'hotel_code': '1ddaf',
              'client_nationality': 'jp',
              'currency': 'USD',
              }

    hotel_list = get_hotel_code_list()

    no_result_counter = 0
    no_pro = 0
    warning = 0
    for i in range(0, 1000):

        hotel = hotel_list[i]
        params['hotel_code'] = hotel
        print i + 1, '.hotel:', hotel_list[i]
        a = Provisi(params)

        resp, avail_pr = a.provision() # Resp is used for provision price

        if resp and avail_pr:
            try:
                avail_pr = float(avail_pr)
                pr = resp.content[80:86] # Price can be taken only using text of response
                pr = float(pr)
                print 'provision price:', pr
                if abs(avail_pr - pr) > 1:
                    print 'WARNINGGGGGGGGGGGGGGGGGGGGG' * 5
                    warning = warning + 1
                else:
                    print 'No Problem'
                    no_pro = no_pro + 1
            except ValueError:
                pr = resp.content[80:85] # Price sometimes can be take place in this interval and
                pr = float(pr) # this method prevents invalid literal error
                if abs(avail_pr - pr) > 1:
                    print 'WARNINGGGGGGGGGGGGGGGGGGGGG' * 5
                    warning = warning + 1
                else:
                    print 'provision price:', pr
                    print 'NP' # No problem
                    no_pro = no_pro + 1

        else:
            print 'No result'
            no_result_counter = no_result_counter + 1

        print '-' * 15
        print 'Total:'
        print 'No problem:', no_pro
        print 'No result:', no_result_counter
        print 'Warning', warning

        print '-' * 60
